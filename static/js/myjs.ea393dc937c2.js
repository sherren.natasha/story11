$.ajax({
	url: "listbook",
	datatype: 'json',
	success: function(result){
		var obj = jQuery.parseJSON(result)
		console.log(obj);
		renderHTML(obj);

	}
})

var container = document.getElementById("demo");
function renderHTML(data){
	htmlstring = "<tbody id='myTable'>";
	for(i = 0; i<data.items.length;i++){
		htmlstring+= "<tr>"+
		"<th scope="+"'row'"+">" + (i+1) + "</th><td>"+
		"<img src='"+data.items[i].volumeInfo.imageLinks.smallThumbnail +"'></img>"+"</td>"
		+ "<td>"+data.items[i].volumeInfo.title +"</td>" +
		"<td>"+data.items[i].volumeInfo.authors +"</td>" +
		"<td>"+data.items[i].volumeInfo.publisher +"</td>"+
		"<td>"+data.items[i].volumeInfo.publishedDate +"</td>"
		+ "<td>"+data.items[i].volumeInfo.categories +"</td>" +
		"<td style="+"'text-align:center'"+">"+
		"<button onclick="+"'favorite(this.id)'"+"id="+"'btn" + i + "' class="+ "'fa fa-star'" + "></button>" + "</td></tr>";
	}
	container.insertAdjacentHTML('beforeend', htmlstring+"</tbody>")
}


var counter = 0;
function favorite(clicked_id){

	var btn = document.getElementById(clicked_id);

	if(btn.classList.contains("checked")){
		btn.classList.remove("checked");
		counter--;
		var count = document.getElementById("counter").innerHTML = counter;


	}
	else{
		btn.classList.add('checked');
		counter++;
		var count = document.getElementById("counter").innerHTML = counter;

	}

}
// Wrap every letter in a span
$('.ml9 .letters').each(function(){
  $(this).html($(this).text().replace(/([^\x00-\x80]|\w)/g, "<span class='letter'>$&</span>"));
});

anime.timeline({loop: true})
  .add({
    targets: '.ml9 .letter',
    scale: [0, 1],
    duration: 1500,
    elasticity: 600,
    delay: function(el, i) {
      return 45 * (i+1)
    }
  }).add({
    targets: '.ml9',
    opacity: 0,
    duration: 1000,
    easing: "easeOutExpo",
    delay: 1000
  });